# OpenID Connect Thumbprint retriever

As mentioned in my blog post [_Getting the OpenID Connect thumbprint for AWS on the command-line with Go_](https://www.jvt.me/posts/2022/05/06/oidc-thumbprint/), it can be handy to be able to retrieve a thumbprint for an OpenID Connect provider.

## Usage

```sh
go install gitlab.com/tanna.dev/oidc-thumbprint@HEAD
oidc-thumbprint https://gitlab.com
```

## License

This code is licensed under Apache-2.0.
